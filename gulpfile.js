import gulp from 'gulp';
import rename from 'gulp-rename';
import { deleteAsync } from 'del';
import autoPrefixer from 'gulp-autoprefixer';
import imagemin from 'gulp-imagemin';
import browserSync from 'browser-sync';
import concat from 'gulp-concat';
import GulpUglify from 'gulp-uglify';

import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);

import pkg from 'gulp';
const { parallel, series } = pkg;

const reset = {
    css: `./dist/*.css`,
    js: `./dist/*.js`,
    img: `./dist/img/**/*.*`
}

const path = {
    scss: './src/scss/*.scss',
    mainScss: './src/scss/main.scss',
    js: './src/js/**/*.js',
    img: './src/img/**/*.*',
    svg: './src/img/**/*.svg',
}

function buildCSS() {
    deleteAsync(reset.css);
    return gulp.src(path.mainScss)
        .pipe(concat('styles.scss'))
        .pipe(sass({
            errorLogToConsole: true,
            outputStyle: "compressed"
        }))
        .pipe(autoPrefixer())
        .pipe(rename({
            suffix: '.min',
            basename: 'styles'
        }))
        .pipe(gulp.dest('./dist/'))
        .pipe(browserSync.stream())
}

function buildJS() {
    deleteAsync(reset.js);
    return gulp.src(path.js)
        .pipe(concat('scripts.js'))
        .pipe(rename({
            suffix: '.min',
            basename: 'scripts'
        }))
        .pipe(GulpUglify())
        .pipe(gulp.dest('./dist/'))
        .pipe(browserSync.stream())
}

function buildCompressIMG() {
    deleteAsync(reset.img);
    return gulp.src(path.img)
        .pipe(gulp.dest('./dist/img'))
        .pipe(imagemin())
        .pipe(gulp.src(path.svg))
        .pipe(gulp.dest('./dist/img'))
        .pipe(browserSync.stream())
}

function watcher() {
    gulp.watch(path.scss, buildCSS).on('change', browserSync.reload);
    gulp.watch(path.js, buildJS).on('change', browserSync.reload);
    gulp.watch('./dist/img/').on('change', browserSync.reload);
    gulp.watch(path.img, buildCompressIMG).on('change', browserSync.reload);
    gulp.watch('./*.html').on('change', browserSync.reload);
}

function server() {
    browserSync.init({
        server: {
            baseDir: './'
        },
        notify: false,
        port: 3000,
    });
}

const build = () => {
    return gulp.parallel(buildCSS, buildJS, buildCompressIMG);
}

const dev = () => {
    return gulp.parallel(watcher, server);
}

gulp.task('default', parallel(dev(), build()));